import { WINDOW } from '@ng-toolkit/universal';
import {
  Component,
  OnDestroy,
  OnInit,
  Inject,
  ViewChild,
  ViewChildren,
  ElementRef,
  AfterViewInit,
  Renderer2,
  QueryList
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CategoryPageService} from '../../services/category-page.service';
import {Meta, Title} from '@angular/platform-browser';
import {HomepageService} from '../../services/homepage.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.less']
})
export class ArticlesComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChildren('adPlacement', {read: ElementRef} ) adPlacement: QueryList<ElementRef>;

  private route_params: any;
  category: string;
  article_id: string;
  each_article_data: any;
  location_url;
  trending_articles = [];
  constructor(@Inject(WINDOW) private window: Window,  private route: ActivatedRoute, private _cps: CategoryPageService, private meta: Meta, private title: Title, private _hps: HomepageService, private _render: Renderer2) {
   this.location_url = typeof window.location  !== 'undefined' ? window.location.href : '';
  }

  ngOnInit() {
    this.route_params = this.route.params.subscribe(params => {
      this.category = params['category'];
      this.article_id = params['id'];
      if (this.window && typeof this.window.scrollTo === 'function') {
        this.window.scrollTo({
          top: 0,
          behavior: 'smooth'
        });
      }
      if (this.window && (this.window as any).gfw_cat_art_info) {
        this.each_article_data = (this.window as any).gfw_cat_art_info[this.category].articles.filter(e => e !== null && e.id == this.article_id)[0];
        this.title.setTitle(this.each_article_data.title + '| Glam Fit Well');
        this.meta.updateTag({
          name: 'author', content: 'glamfitwell.com',
        });
        this.meta.updateTag({
          name: 'keywords', content: 'glam, glamorous, diva, beauty, beauty tips, nutrition, relationship, love, hair, skin, lifestyle, wellness, health, daily care, fitness, girls',
        });
        this.meta.updateTag({
          name: 'description', content: this.each_article_data.description,
        });
        this.meta.updateTag({
          property: 'og:title', content: this.each_article_data.title,
        });
        this.meta.updateTag({
          property: 'og:url', content: `'` + this.location_url + `'`,
        });
        this.meta.updateTag({
          property: 'og:image', content: this.each_article_data.image
        });
        this.meta.updateTag({ property: 'og:image:type', content: 'image/png' });
        const article_categories = (this.window as any).gfw_cat_art_info;
        const top_articles_stack = Object.values(article_categories);
        top_articles_stack.forEach(category => {
          if (typeof category['articles'] !== 'undefined') {
            const length_of_articles = category['articles'].length;
            const articles = category['articles'];
            if (articles.length > 0) {
              articles.forEach( e => {
                if ( articles.indexOf(e) === length_of_articles - 1 || articles.indexOf(e) === length_of_articles - 2 || articles.indexOf(e) === length_of_articles - 3) {
                  this.trending_articles.push(e);
                }
              });
            }
          }
        });
      } else {
        this.getArticleData(this.category, this.article_id);
      }
    });
  }

  ngAfterViewInit() {
    console.log(this.adPlacement);
    this.adPlacement.changes.subscribe(e => {
      const mn_script = this._render.createElement('script');
      mn_script.id = 'mNCC';
      mn_script.innerHTML = `
        medianet_width = "160";
        medianet_height = "600";
        medianet_crid = "721111010";
        medianet_versionId = "3111299";
    `;
      mn_script.language = 'javascript';

      const mn_url = this._render.createElement('script');
      mn_url.src = '//contextual.media.net/nmedianet.js?cid=8CURP830V';

      this.adPlacement.first.nativeElement.insertBefore(mn_script, this.adPlacement.first.nativeElement.firstChild);
      this.adPlacement.first.nativeElement.insertBefore(mn_url, this.adPlacement.first.nativeElement.firstChild);
    });

  }

  ngOnDestroy() {
    if (typeof  this.route_params !== 'undefined') {
      this.route_params.unsubscribe();
    }
  }

  getArticleData(category, id) {
    this._cps.getArticleData(category, id)
      .subscribe((response) => {
        const body = response;
        this.each_article_data = body;
        this.title.setTitle(this.each_article_data.title + '| GlamFitWell');
        this.meta.updateTag({
          name: 'author', content: 'glamfitwell.com',
        });
        this.meta.updateTag({
          name: 'keywords', content: 'glam, glamorous, diva, beauty, beauty tips, nutrition, relationship, love, hair, skin, lifestyle, wellness, health, daily care, fitness, girls',
        });
        this.meta.updateTag({
          name: 'description', content: this.each_article_data.description,
        });
        this.meta.updateTag({
          property: 'og:title', content: this.each_article_data.title,
        });
        this.meta.updateTag({
          property: 'og:url', content: `'` + this.location_url + `'`,
        });
        this.meta.updateTag({
          property: 'og:image', content: this.each_article_data.image
        });
        this.meta.updateTag({ property: 'og:image:type', content: 'image/png' });
      });

    this._hps.getCategories()
      .subscribe((response) => {
        const body = response;
        if (this.window) {
          (this.window as any).gfw_cat_art_info = body;
          const article_categories = (this.window as any).gfw_cat_art_info;
          const top_articles_stack = Object.values(article_categories);
          top_articles_stack.forEach(category => {
            if (typeof category['articles'] !== 'undefined') {
              const length_of_articles = category['articles'].length;
              const articles = category['articles'];
              if (articles.length > 0) {
                articles.forEach( e => {
                  if ( articles.indexOf(e) === length_of_articles - 1 || articles.indexOf(e) === length_of_articles - 2 || articles.indexOf(e) === length_of_articles - 3) {
                    this.trending_articles.push(e);
                  }
                });
              }
            }
          });
        }
      });
  }

}
