import { WINDOW } from '@ng-toolkit/universal';
import {Component, OnInit, Inject, ViewChild, ElementRef, AfterViewInit, Renderer2, ViewChildren, QueryList} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CategoryPageService} from '../../services/category-page.service';
import {Meta, Title} from '@angular/platform-browser';
import {HomepageService} from '../../services/homepage.service';
@Component({
  selector: 'app-slide-show-page',
  templateUrl: './slide-show-page.component.html',
  styleUrls: ['./slide-show-page.component.less']
})
export class SlideShowPageComponent implements OnInit, AfterViewInit {
  @ViewChildren('adPlacement', {read: ElementRef} ) adPlacement: QueryList<ElementRef>;

  private route_params: any;
  category: string;
  article_id: string;
  slides_data: any;
  slides_metadata: any;
  slide_id: any;
  counter = 0;
  location_url;

  constructor(@Inject(WINDOW) private window: Window, private route: ActivatedRoute, private _router: Router, private meta: Meta, private title: Title, private _hps: HomepageService, private _cps: CategoryPageService, private _render: Renderer2) {
    this.location_url = typeof window.location  !== 'undefined' ? window.location.href : '';
  }

  ngOnInit() {
    if (this.window && typeof this.window.scrollTo === 'function') {
      this.window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    }
    this.route_params = this.route.params.subscribe(params => {
      this.category = params['category'];
      this.article_id = params['id'];
      this.slide_id = params['slide_id'] != undefined ? parseInt(params['slide_id']) : params['slide_id'];

      if (this.window && (this.window as any).gfw_cat_art_info) {
        this.slides_data = (this.window as any).gfw_cat_art_info[this.category].articles.filter(e => e !== null && e.id == this.article_id)[0];
        this.slides_metadata = this.slides_data.metadata.content.filter(e => e !== null);
        this.title.setTitle(this.slides_data.title + '| GlamFitWell');
        this.meta.updateTag({
          name: 'author', content: 'glamfitwell.com',
        });
        this.meta.updateTag({
          name: 'keywords', content: 'glam, glamorous, diva, beauty, beauty tips, nutrition, relationship, love, hair, skin, lifestyle, wellness, health, daily care, fitness, girls',
        });
        this.meta.updateTag({
          name: 'description', content: this.slides_data.description,
        });
        this.meta.updateTag({
          property: 'og:title', content: this.slides_data.title,
        });
        this.meta.updateTag({
          property: 'og:url', content: `'` + this.location_url + `'`,
        });
        this.meta.updateTag({
          property: 'og:image', content: this.slides_data.image
        });
        this.meta.updateTag({ property: 'og:image:type', content: 'image/png' });
      } else {
        this.getSlidesData(this.category, this.article_id);
      }
    });
  }

  ngAfterViewInit() {
    this.adPlacement.changes.subscribe(e => {
      const mn_script = this._render.createElement('script');
      mn_script.id = 'mNCC';
      mn_script.innerHTML = `
        medianet_width = "160";
        medianet_height = "600";
        medianet_crid = "721111010";
        medianet_versionId = "3111299";
    `;
      mn_script.language = 'javascript';

      const mn_url = this._render.createElement('script');
      mn_url.src = '//contextual.media.net/nmedianet.js?cid=8CURP830V';

      this.adPlacement.first.nativeElement.insertBefore(mn_script, this.adPlacement.first.nativeElement.firstChild);
      this.adPlacement.first.nativeElement.insertBefore(mn_url, this.adPlacement.first.nativeElement.firstChild);
    });
  }

  startCounter() {
    this.slide_id = 1;
    this._router.navigate([this.category, this.article_id, 'slides', this.slide_id]);
  }
  slideIncrement() {
    if (this.slide_id !== this.slides_metadata.length) {
      this.slide_id++;
      this._router.navigate([this.category, this.article_id, 'slides', this.slide_id]);
    }
  }
  slideDecrement() {
    if (this.slide_id > 1) {
      this.slide_id--;
      this._router.navigate([this.category, this.article_id, 'slides', this.slide_id]);
    } else if (this.slide_id === 1) {
      this._router.navigate([this.category, this.article_id, 'slides']);
    }
  }
  getSlidesData(category, id) {
    this._cps.getArticleData(category, id)
      .subscribe((response) => {
        const body = response;
        this.slides_data = body;
        this.slides_metadata = this.slides_data.metadata.content.filter(e => e !== null);
        this.title.setTitle(this.slides_data.title + '| Glam Fit Well');
        this.meta.updateTag({
          name: 'author', content: 'glamfitwell.com',
        });
        this.meta.updateTag({
          name: 'keywords', content: 'glam, glamorous, diva, beauty, beauty tips, nutrition, relationship, love, hair, skin, lifestyle, wellness, health, daily care, fitness, girls',
        });
        this.meta.updateTag({
          name: 'description', content: this.slides_data.description,
        });
        this.meta.updateTag({
          property: 'og:title', content: this.slides_data.title,
        });
        this.meta.updateTag({
          property: 'og:url', content: `'` + this.location_url + `'`,
        });
        this.meta.updateTag({
          property: 'og:image', content: this.slides_data.image
        });
        this.meta.updateTag({ property: 'og:image:type', content: 'image/png' });
      });
    this._hps.getCategories()
      .subscribe((response) => {
        const body = response;
        if (this.window) {
          (this.window as any).gfw_cat_art_info = body;
        }
      });
  }

}
