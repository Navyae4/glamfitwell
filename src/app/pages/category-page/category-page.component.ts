import { WINDOW } from '@ng-toolkit/universal';
import {Component, OnDestroy, OnInit, Inject} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CategoryPageService} from '../../services/category-page.service';
import {Meta, Title} from '@angular/platform-browser';
import {HomepageService} from '../../services/homepage.service';
interface ResponseArticles {
  articles: any;
  mainImage: string;
}

@Component({
  selector: 'app-category-page',
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.less']
})
export class CategoryPageComponent implements OnInit, OnDestroy {
  private sub: any;
  category: string;
  article_data: any;
  category_image: string;
  location_url;
  constructor(@Inject(WINDOW) private window: Window,  private route: ActivatedRoute, private _cps: CategoryPageService, private meta: Meta, private title: Title, private _hps: HomepageService) {
    this.location_url = typeof window.location  !== 'undefined' ? window.location.href : '';
  }

  ngOnInit() {
    if (this.window && typeof this.window.scrollTo === 'function') {
      this.window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    }
    this.sub = this.route.params.subscribe(params => {
      this.category = params['category'];
      if (this.category !== 'privacy' && this.category !== 'tos' && this.category !== 'cookies' && this.category !== 'about') {
        this.getCategoryArticles(this.category);
        this.title.setTitle('Glam Fit Well ' + this.category + ' Page');
        this.meta.updateTag({
          name: 'author', content: 'glamfitwell.com',
        });
        this.meta.updateTag({
          name: 'keywords', content: 'glam, glamorous, diva, beauty, beauty tips, nutrition, relationship, love, hair, skin, lifestyle, wellness, health, daily care, fitness, girls',
        });
        this.meta.updateTag({
          name: 'description', content: 'All about beauty, wellness, fitness and health',
        });
        this.meta.updateTag({
          property: 'og:title', content: 'GlamFitWell ' + this.category + ' Page',
        });
        this.meta.updateTag({
          property: 'og:url', content: `'` + this.location_url + `'`,
        });
        this.meta.updateTag({
          property: 'og:image', content: this.category_image,
        });
        this.meta.updateTag({ property: 'og:image:type', content: 'image/png' });
      }
    });
  }

  ngOnDestroy() {
    if(typeof this.sub !== 'undefined') {
      this.sub.unsubscribe();
    }
  }

  getCategoryArticles(category) {
    if (this.window && (this.window as any).gfw_cat_art_info) {
      const articles_by_category = (this.window as any).gfw_cat_art_info[category];
      if (typeof articles_by_category.articles !== 'undefined') {
        this.article_data = articles_by_category.articles.filter(e => e !== null);
        this.category_image = articles_by_category.mainImage;
      }
    } else {
      this._cps.getArticlesByCategory(category)
        .subscribe((response: ResponseArticles) => {
          const body = response;
          if (body.articles) {
            this.article_data = body.articles.filter(e => e !== null);
          }
          this.category_image = body.mainImage;
        });
      this._hps.getCategories()
        .subscribe((response) => {
          const body = response;
          if (this.window) {
            (this.window as any).gfw_cat_art_info = body;
          }
        });
    }

  }

}
