import {Component, OnInit} from '@angular/core';
import {HomepageService} from './services/homepage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  constructor(private _hps: HomepageService) {}

  ngOnInit() {

  }
}
