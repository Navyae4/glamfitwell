import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticlesComponent } from '../pages/articles/articles.component';
import {RouterModule} from '@angular/router';
import {CategoryPageService} from '../services/category-page.service';
import {GlobalModule} from '../global/global.module';

@NgModule({
  imports: [
    CommonModule,
    GlobalModule,
    RouterModule.forChild([
      { path: '', component: ArticlesComponent, pathMatch: 'full'}
    ])
  ],
  declarations: [ArticlesComponent],
  providers: [CategoryPageService]
})
export class ArticlePageModule { }
