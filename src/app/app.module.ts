import { NgtUniversalModule } from '@ng-toolkit/universal';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { PLATFORM_ID, APP_ID, Inject } from '@angular/core';
import { isPlatformBrowser , CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {HomepageService} from './services/homepage.service';
import { HomeComponent } from './home/home.component';
import { HomeCanvasComponent } from './components/home-canvas/home-canvas.component';
import { HomeCategoriesComponent } from './components/home-categories/home-categories.component';
import {RouterModule} from '@angular/router';
import {GlobalModule} from './global/global.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HomeCanvasComponent,
    HomeCategoriesComponent
  ],
  imports: [
    GlobalModule,
    CommonModule,
    NgtUniversalModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [HomepageService],
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string) {
    const platform = isPlatformBrowser(platformId) ?
      'in the browser' : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
