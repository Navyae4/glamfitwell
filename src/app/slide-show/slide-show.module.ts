import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlideShowPageComponent } from '../pages/slide-show-page/slide-show-page.component';
import {RouterModule} from '@angular/router';
import {CategoryPageService} from '../services/category-page.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SlideShowPageComponent, pathMatch: 'full'},
      { path: ':slide_id', component: SlideShowPageComponent}
    ])
  ],
  declarations: [SlideShowPageComponent],
  providers: [CategoryPageService]
})
export class SlideShowModule { }
