import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryPageComponent } from '../pages/category-page/category-page.component';
import {CategoryPageService} from '../services/category-page.service';
import {CookiePolicyComponent} from '../components/cookie-policy/cookie-policy.component';
import {PrivacyPolicyComponent} from '../components/privacy-policy/privacy-policy.component';
import {TermsofserviceComponent} from '../components/termsofservice/termsofservice.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: CategoryPageComponent, pathMatch: 'full'},
    ]),
  ],
  declarations: [
    CategoryPageComponent,
    CookiePolicyComponent,
    PrivacyPolicyComponent,
    TermsofserviceComponent
  ],
  providers: [CategoryPageService],

})
export class CategoryArticlesModule { }
