import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import {HttpClient } from '@angular/common/http';
interface Canvas {
  image1: string;
  image2: string;
  image3: string;
}

@Component({
  selector: 'app-home-canvas',
  templateUrl: './home-canvas.component.html',
  styleUrls: ['./home-canvas.component.less']
})

export class HomeCanvasComponent implements OnInit {
  image1: string;
  image2: string;
  image3: string;
  constructor(@Inject(WINDOW) private window: Window,  private _http: HttpClient) { }

  ngOnInit() {
    if ((this.window as any).gfw_canvas) {
      const canvas_data = (this.window as any).gfw_canvas;
      this.image1 = canvas_data.image1;
      this.image2 = canvas_data.image2;
      this.image3 = canvas_data.image3;

    } else {
      this._http.get('https://bn-glam.firebaseio.com/canvas.json')
        .subscribe((data: any) => {
          (this.window as any).gfw_canvas = data;
          this.image1 = data.image1;
          this.image2 = data.image2;
          this.image3 = data.image3;

        });
    }
  }

}
