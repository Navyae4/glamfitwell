import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-categories',
  templateUrl: './home-categories.component.html',
  styleUrls: ['./home-categories.component.less']
})
export class HomeCategoriesComponent implements OnInit {
  @Input() articles: any;
  @Input() category_keys: any;
  constructor() { }

  ngOnInit() {
  }

}
