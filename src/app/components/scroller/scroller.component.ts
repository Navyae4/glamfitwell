import {Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, Renderer2} from '@angular/core';

@Component({
  selector: 'app-scroller',
  templateUrl: './scroller.component.html',
  styleUrls: ['./scroller.component.less']
})
export class ScrollerComponent implements OnInit, AfterViewInit {
  @Input() articlesList: any;
  @ViewChild('trending_scroller', {read: ElementRef}) trending_scroller: ElementRef;
  @ViewChild('left_arrow', {read: ElementRef}) t_arrow_left: ElementRef;
  @ViewChild('right_arrow', {read: ElementRef}) t_arrow_right: ElementRef;

  scroll_element;
  left_click_element;
  right_click_element;

  constructor(private _eref: ElementRef, private _render: Renderer2) { }

  ngOnInit() {
  }
  ngAfterViewInit(): void {
    this.scroll_element = this.trending_scroller.nativeElement;
    this.left_click_element = this.t_arrow_left.nativeElement;
    this.right_click_element = this.t_arrow_right.nativeElement;
  }

  scrollerLeftClick(e) {
    if (this.scroll_element !== undefined && this.left_click_element !== undefined && this.right_click_element !== undefined) {
      const scroll_left = this.scroll_element.scrollLeft;
      const scroll_width = this.scroll_element.scrollWidth;
      const client_width = this.scroll_element.clientWidth;
      if (scroll_left !== 0 && scroll_left >= client_width ) {
        this._render.setStyle(this.right_click_element, 'color', 'black');
        this._render.setProperty(this.scroll_element, 'scrollLeft', scroll_left - client_width );
      } else {
        this._render.setProperty(this.scroll_element, 'scrollLeft', 0);
        this._render.setStyle(this.left_click_element, 'color', '#ccc');
      }
    }

  }
  scrollerRightClick(e) {
    if (this.scroll_element !== undefined && this.left_click_element !== undefined && this.right_click_element !== undefined) {
      const scroll_left = this.scroll_element.scrollLeft;
      const scroll_width = this.scroll_element.scrollWidth;
      const client_width = this.scroll_element.clientWidth;
      if (scroll_left + client_width !== scroll_width) {
        this._render.setStyle(this.left_click_element, 'color', 'black');
        this._render.setProperty(this.scroll_element, 'scrollLeft', scroll_left + client_width);
      } else {
        this._render.setStyle(this.right_click_element, 'color', '#ccc');
      }
    }
  }

}
