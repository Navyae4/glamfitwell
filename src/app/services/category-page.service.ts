import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';

@Injectable()

export class CategoryPageService {
    constructor(private _http: HttpClient) {}

    getArticlesByCategory(article_type) {
      const url = `https://bn-glam.firebaseio.com/categories/${article_type}.json`;
      return this._http.get(url);
    }

    getArticleData(category, id) {
      const url = `https://bn-glam.firebaseio.com/categories/${category}/articles/${id}.json`;
      return this._http.get(url);
    }

}
