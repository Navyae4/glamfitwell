import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
@Injectable()
export class HomepageService {
  constructor(private _http: HttpClient) {}

  getCategories() {
      const url = 'https://bn-glam.firebaseio.com/categories.json';
      return this._http.get(url);
  }
}
