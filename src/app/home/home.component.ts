import { WINDOW } from '@ng-toolkit/universal';
import { Component, OnInit , Inject} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HomepageService} from '../services/homepage.service';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  article_categories;
  category_keys;
  top_articles_stack;
  trending_articles = [];
  constructor(@Inject(WINDOW) private window: Window, private _http: HttpClient, private _hps: HomepageService, private meta: Meta, private title: Title) {
    const location_url = typeof window.location  !== 'undefined' ? window.location.href : '';
    title.setTitle('Glam Fit Well Home Page');
    this.meta.updateTag({
      name: 'author', content: 'glamfitwell.com',
    });
    this.meta.updateTag({
      name: 'keywords', content: 'glam, glamorous, diva, beauty, beauty tips, nutrition, relationship, love, hair, skin, lifestyle, wellness, health, daily care, fitness, girls',
    });
    this.meta.updateTag({
      name: 'description', content: 'All about beauty, wellness, fitness and health',
    });
    this.meta.updateTag({
      property: 'og:title', content: 'GlamFitWell Home Page',
    });
    this.meta.updateTag({
      property: 'og:url', content: `'` + location_url + `'`,
    });
    this.meta.updateTag({
      property: 'og:image', content: null,
    });
    this.meta.updateTag({ property: 'og:image:type', content: 'image/png' });
  }

  ngOnInit() {
    if (this.window && typeof this.window.scrollTo === 'function') {
      this.window.scrollTo({
        top: 0,
        behavior: 'smooth'
      });
    }
    if (this.window && (this.window as any).gfw_cat_art_info) {
      this.article_categories = (this.window as any).gfw_cat_art_info;
      this.category_keys = Object.keys(this.article_categories);
      this.top_articles_stack = Object.values(this.article_categories);
      this.top_articles_stack.forEach(category => {
        if (typeof category.articles !== 'undefined') {
          const length_of_articles = category.articles.length;
          const articles = category.articles;
          if (articles.length > 0) {
            articles.forEach( e => {
              if ( articles.indexOf(e) === length_of_articles - 1 || articles.indexOf(e) === length_of_articles - 2 || articles.indexOf(e) === length_of_articles - 3) {
                this.trending_articles.push(e);
              }
            });
          }
        }
      });
    } else {

      this._hps.getCategories()
        .subscribe((response) => {
          const body = response;
          if (this.window) {
            (this.window as any).gfw_cat_art_info = body;
          }
          this.article_categories = body;
          this.category_keys = Object.keys(this.article_categories);
          this.top_articles_stack = Object.values(this.article_categories);
          this.top_articles_stack.forEach(category => {
            if (typeof category.articles !== 'undefined') {
              const length_of_articles = category.articles.length;
              const articles = category.articles;
              if (articles.length > 0) {
                articles.forEach( e => {
                  if ( articles.indexOf(e) === length_of_articles - 1 || articles.indexOf(e) === length_of_articles - 2 || articles.indexOf(e) === length_of_articles - 3) {
                    this.trending_articles.push(e);
                  }
                });
              }
            }
          });
        });
    }

  }
}
