import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full'},
  { path: ':category', loadChildren: './category-articles/category-articles.module#CategoryArticlesModule'},
  { path: ':category/:id', loadChildren: './article-page/article-page.module#ArticlePageModule'},
  {path: ':category/:id/slides', loadChildren: './slide-show/slide-show.module#SlideShowModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
