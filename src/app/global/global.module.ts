import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ScrollerComponent} from '../components/scroller/scroller.component';
import {AppRoutingModule} from '../app-routing.module';
import {HomepageService} from '../services/homepage.service';

@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  declarations: [ScrollerComponent],
  exports: [ScrollerComponent],
  providers:[HomepageService]
})
export class GlobalModule { }
